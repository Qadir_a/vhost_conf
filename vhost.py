#! usr/bin/python3

import os
import subprocess
import sys


vhost_tmplt = """
<VirtualHost *:80>
	ServerAdmin qadirsanny@gmail.com
	ServerName {base_url}
	ServerAlias www.{base_url}
	DocumentRoot {base_path}
	ErrorLog ${{APACHE_LOG_DIR}}/error.log
	CustomLog ${{APACHE_LOG_DIR}}/access.log combined
	<Directory {base_path}>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Require all granted
	</Directory>
RewriteEngine on
RewriteCond %{{SERVER_NAME}} =www.{base_url} [OR]
RewriteCond %{{SERVER_NAME}} ={base_url}
RewriteRule ^ https://%{{SERVER_NAME}}%{{REQUEST_URI}} [END,NE,R=permanent]
</VirtualHost>
"""

print(sys.argv)
args = sys.argv[1:]
site_url = args[0]
site_root_path = args[1]

base_apache_sites_conf = '/etc/apache2/sites-available'
base_apache_sites_path = '/var/www'

vhostfile = f"{site_url}.conf"
vhostfilepath = f'{base_apache_sites_conf}/{vhostfile}'

base_path = f'{base_apache_sites_path}/{site_root_path}'
with open(vhostfilepath, "w+") as vhost:
    vhost.write(vhost_tmplt.format(base_path=base_path, base_url=site_url))

subprocess.call(["a2ensite", vhostfile])
subprocess.call("systemctl reload apache2".split(' '))
subprocess.call(f"certbot --apache -d {site_url}".split(' '))
